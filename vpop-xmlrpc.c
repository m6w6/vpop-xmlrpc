/**
 * vpop-xmlrpc
 *
 *
 * $Id$
 */


/* {{{ includes */

#ifdef HAVE_CONFIG_H
#   include "vpop-xmlrpc-config.h"
#endif

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iconv.h>
#include <ctype.h>

#include <vpopmail.h>
#include <vauth.h>

#include <xmlrpc.h>
#include <xmlrpc_abyss.h>
#include <xmlrpc_cgi.h>
/* }}} */

/* {{{ MACROS */
#ifndef VPOP_XMLRPC_APOP
#define VPOP_XMLRPC_APOP 0
#endif

#ifndef VPOP_XMLRPC_ENCODING
#define VPOP_XMLRPC_ENCODING "ISO-8859-1"
#endif

#define VPOP_XMLRPC_E_ICONV              10000
#define VPOP_XMLRPC_E_ASSIGN             10001
#define VPOP_XMLRPC_E_BADCALL            10002
#define VPOP_XMLRPC_E_BADPASS            10003
#define VPOP_XMLRPC_E_SETGID             10004
#define VPOP_XMLRPC_E_SETUID             10005

#ifdef vauth_open
#   define VAUTH_CLOSE()    vauth_close()
#   define VAUTH_OPEN(mod)  CHECH_FAULT(vauth_open(mod), 0, "vauth_open() failed")
#else
#   define VAUTH_CLOSE()
#   define VAUTH_OPEN(mod)
#endif

#define CHECK_LONG_PASS(pass) \
    if (strlen(pass) >= MAX_PW_CLEAR_PASSWD) { \
        password[MAX_PW_CLEAR_PASSWD - 1] = 0; \
    }
#define CHECK_SANE_PASS(pass) \
    { \
        unsigned int i = 0; \
        while (pass[i]) { \
            if (isspace(pass[i]) && (pass[i] != ' ')) { \
                xmlrpc_env_set_fault(env, VPOP_XMLRPC_E_BADPASS, "Password contains new lines or tabs characters."); \
                break; \
            } \
            ++i; \
        } \
    }
#ifdef CLEAR_PASS
#   define CHECK_PASS(pass) \
        CHECK_LONG_PASS(pass); \
        CHECK_SANE_PASS(pass)
#else
#   define CHECK_PASS(pass) \
        CHECK_SANE_PASS(pass)
#endif

#define RETURN_TRUE \
    return xmlrpc_build_value(env, "b", (xmlrpc_bool) 1)
#define RETURN_FALSE \
    return xmlrpc_build_value(env, "b", (xmlrpc_bool) 0)

#define IF_FAULT() \
    if (env->fault_occurred)

#define RETURN_FAULT(ret) \
    IF_FAULT() return ret

#define RAISE_FAULT(code, message) \
     xmlrpc_env_set_fault(env, code, message);
#define CHECK_FAULT(check, code, message) \
    if (check) { \
        RAISE_FAULT(code, message); \
    }
#define CHECK_VFAULT(check) \
    { \
        int error; \
        if (error = check) { \
            RAISE_FAULT(error * -1, verror(error)); \
        } \
    }

#define ICONV_DEFT \
    iconv_t _CD
#define ICONV_PASS \
    _CD
#define ICONV_OPEN(open_for_output) \
    if (open_for_output) { \
        _CD = iconv_open("UTF-8", VPOP_XMLRPC_ENCODING); \
    } else { \
        _CD = iconv_open(VPOP_XMLRPC_ENCODING, "UTF-8"); \
    } \
    if (_CD == (iconv_t) -1) { \
       RAISE_FAULT(VPOP_XMLRPC_E_ICONV, "Failed to initlialize iconv."); \
    }
#define ICONV_CLOSE() \
    iconv_close(_CD)
#define ICONV(in) \
    ICONV_EXEC(in, 0)
#define ICONV_FREE(in) \
    ICONV_EXEC(in, 1)
#define ICONV_EXEC(in, dofree) \
    { \
        char *out, *out_ptr, *in_ptr = in; \
        size_t ret_val; \
        size_t in_len = strlen(in); \
        size_t out_len = 2 * in_len * sizeof(int); \
        out = calloc(1, out_len + 1); \
        out_ptr = out; \
        ret_val = iconv(_CD, &in_ptr, &in_len, &out_ptr, &out_len); \
        if (ret_val == (size_t) -1) { \
            xmlrpc_env_set_fault_formatted(env, VPOP_XMLRPC_E_ICONV, "Failed to en/decode string: '%s'.", in); \
        } else { \
            if (dofree) { \
                free(in); \
            } \
            in = out; \
        } \
    }
/* }}} */

/* {{{ typedefs etc. */
typedef xmlrpc_value *(xmlrpc_method_t)(xmlrpc_env *, xmlrpc_value *, void *);

static xmlrpc_method_t vpop_adduser;
static xmlrpc_method_t vpop_deluser;
static xmlrpc_method_t vpop_passwd;
static xmlrpc_method_t vpop_setquota;
static xmlrpc_method_t vpop_auth;
static xmlrpc_method_t vpop_checkauth;
static xmlrpc_method_t vpop_listusers;
static xmlrpc_method_t vpop_listdomains;
static xmlrpc_method_t vpop_listuser;
static xmlrpc_method_t vpop_listdomain;
static xmlrpc_method_t vpop_moduser;
static xmlrpc_method_t vpop_setflags;
static xmlrpc_method_t vpop_listflags;
static xmlrpc_method_t vpop_batch;

struct xmlrpc_method_entry {
    char            *name;
    xmlrpc_method_t *func;
    void            *data;
    char            *spec;
    char            *docs;
};

/* available vpop.* methods */
static struct xmlrpc_method_entry me[] = {
    {"vpop.adduser",        vpop_adduser,       NULL,   "b:S",  "Add a vpopmail user to a domain with password and fullname."},
    {"vpop.deluser",        vpop_deluser,       NULL,   "b:S",  "Delete a user from a domain."},
    {"vpop.passwd",         vpop_passwd,        NULL,   "b:S",  "Change the password of a user in a domain."},
    {"vpop.setquota",       vpop_setquota,      NULL,   "b:S",  "Set the quota of a user in a domain."},
    {"vpop.auth",           vpop_auth,          NULL,   "b:S",  "Authenticate a user in a domain with the password."},
    {"vpop.pass",           vpop_checkauth,     NULL,   "b:S",  "Identical to vpop.auth except that auth logging is *NOT* done."},
    {"vpop.listusers",      vpop_listusers,     NULL,   "S:S",  "List limit count users of a domain starting at offset and sort them optionally."},
    {"vpop.listdomains",    vpop_listdomains,   NULL,   "S:",   "List all domains."},
    {"vpop.listuser",       vpop_listuser,      NULL,   "S:S",  "List all available information of a user of a domain."},
    {"vpop.listdomain",     vpop_listdomain,    NULL,   "S:S",  "List all available information of a domain."},
    {"vpop.moduser",        vpop_moduser,       NULL,   "S:b",  "Modify the fullname or flags of a user in a domain."},
    {"vpop.setflags",       vpop_setflags,      NULL,   "b:S",  "Set the flags of a user in a domain."},
    {"vpop.listflags",      vpop_listflags,     NULL,   "S:",   "List all available flags and their values."},
    {"vpop.batch",          vpop_batch,         NULL,   "S:S",  "Batch processor."},
};

struct vpop_xmlrpc_flag {
    char      *name;
    size_t    nlen;
    int       flag;
};

static struct vpop_xmlrpc_flag flags[] = {
    {"no_password_change",      sizeof("no_password_change")-1,     NO_PASSWD_CHNG},
    {"no_pop_access",           sizeof("no_pop_access")-1,          NO_POP},
    {"no_smtp_auth_access",     sizeof("no_smtp_auth_access")-1,    NO_SMTP},
    {"no_local_imap_access",    sizeof("no_local_imap_access")-1,   NO_WEBMAIL},
    {"no_remote_imap_access",   sizeof("no_remote_imap_access")-1,  NO_IMAP},
    {"do_bounce_all",           sizeof("do_bounce_all")-1,          BOUNCE_MAIL},
    {"no_domain_limits",        sizeof("no_domain_limits")-1,       V_OVERRIDE},
    {"no_roaming",              sizeof("no_roaming")-1,             NO_RELAY},
    {"is_qmail_admin",          sizeof("is_qmail_admin")-1,         QA_ADMIN},
#ifdef SA_ADMIN
    {"is_system_admin",         sizeof("is_system_admin")-1,        SA_ADMIN},
#endif
#ifdef SA_EXPERT
    {"is_expert_admin",         sizeof("is_expert_admin")-1,        SA_EXPERT},
#endif
#ifdef NO_SPAMASSASSIN
    {"no_spamassassin",         sizeof("no_spamassassin")-1,        NO_SPAMASSASSIN},
#endif
#ifdef DELETE_SPAM
    {"do_delete_spam",          sizeof("do_delete_spam")-1,         DELETE_SPAM},
#endif
};

/* needed for open_big_dir (to read out count of users in a domain) */
extern vdir_type vdir;

/* domain entries */
struct domentry {
    char *name;
    char *real;
    int uid;
    int gid;
    int alias;
    int users;
    struct domentry *next;
};
/* }}} */

/* {{{ forward declarations */

/* missing in some xmlrpc.h versions */
#ifndef xmlrpc_array_new
#   define xmlrpc_array_new(env) xmlrpc_build_value(env, "()")
#endif
#ifndef xmlrpc_string_new
#   define xmlrpc_string_new(env, s) xmlrpc_build_value(env, "s", s)
#endif
#ifndef xmlrpc_bool_new
#   define xmlrpc_bool_new(env, b) xmlrpc_build_value(env, "b", b)
#endif
#ifndef xmlrpc_int_new
#   define xmlrpc_int_new(env, i) xmlrpc_build_value(env, "i", i)
#endif
#ifdef xmlrpc_array_read_item
#   define xmlrpc_array_item(env, array, i, in) xmlrpc_array_read_item(env, array, i, &in)
#else
#   define xmlrpc_array_item(env, array, i, in) in = xmlrpc_array_get_item(env, array, i)
#endif
/* --- */

static struct xmlrpc_method_entry *me_entry(xmlrpc_env *env, char *name);

static inline int batch_fault(xmlrpc_env *env, xmlrpc_int32 c, xmlrpc_value *array);

static struct domentry *get_domain(xmlrpc_env *env, char *domain);
static inline void free_domentry(struct domentry *e);
static inline int get_domusers(char *domain, uid_t uid, gid_t gid);

static inline xmlrpc_value *encode_pw(xmlrpc_env *env, ICONV_DEFT, char *domain, struct vqpasswd *mypw, xmlrpc_value *into_struct);
static inline xmlrpc_value *encode_de(xmlrpc_env *env, ICONV_DEFT, struct domentry *de, xmlrpc_value *into_struct);

static inline xmlrpc_value *auth(xmlrpc_env *env, xmlrpc_value *param, void *data, int log_auth);
/* }}} */

/*
 * === API ===
 */

/* {{{ vpop.batch */
static xmlrpc_value *vpop_batch(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    char *method;
    xmlrpc_int32 vcount, i;
    xmlrpc_value *values, *ret, *faults, *results;
    struct xmlrpc_method_entry *e;

    xmlrpc_parse_value(env, param, "({s:s,s:A,*})",
            "method", &method,
            "values", &values
    );

    RETURN_FAULT(NULL);
    
    vcount = xmlrpc_array_size(env, values);
    RETURN_FAULT(NULL);
    
    e = me_entry(env, method);
    RETURN_FAULT(NULL);

    ret = xmlrpc_struct_new(env);
    RETURN_FAULT(NULL);
    faults = xmlrpc_array_new(env);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, ret, "faults", faults);
    RETURN_FAULT(NULL);
    results = xmlrpc_array_new(env);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, ret, "results", results);
    RETURN_FAULT(NULL);
    
    for (i = 0; i < vcount; ++i) {
        xmlrpc_value *out, *in, *tmparr;
        
        xmlrpc_array_item(env, values, i, in);
        IF_FAULT() {
            if (batch_fault(env, i, faults)) {
                RETURN_FAULT(NULL);
            }
        } else {
            tmparr = xmlrpc_array_new(env);
            RETURN_FAULT(NULL);
            xmlrpc_array_append_item(env, tmparr, in);
            RETURN_FAULT(NULL);
        
            out = e->func(env, tmparr, e->data);
            IF_FAULT() {
                if (batch_fault(env, i, faults)) {
                    RETURN_FAULT(NULL);
                }
            } else {
                xmlrpc_array_append_item(env, results, out);
                RETURN_FAULT(NULL);
            }
        }
    }
    
    return ret;
}
/* }}} */

/* {{{ vpop.adduser */
static xmlrpc_value *vpop_adduser(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    char *user, *domain, *password, *fullname;
    ICONV_DEFT;
    int vrs;

    xmlrpc_parse_value(env, param, "({s:s,s:s,s:s,s:s,*})",
            "user",     &user,
            "domain",   &domain,
            "password", &password,
            "fullname", &fullname
    );
    RETURN_FAULT(NULL);
    
    VAUTH_OPEN(1);
    RETURN_FAULT(NULL);
    
    ICONV_OPEN(0);
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(user);
    IF_FAULT() {
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(domain);
    IF_FAULT() {
        free(user);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(password);
    IF_FAULT() {
        free(user);
        free(domain);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(fullname);
    IF_FAULT() {
        free(user);
        free(domain);
        free(password);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_PASS(password);
    IF_FAULT() {
        free(user);
        free(domain);
        free(password);
        free(fullname);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_VFAULT(vadduser(user, domain, password, fullname, VPOP_XMLRPC_APOP));
    
    ICONV_CLOSE();
    VAUTH_CLOSE();
    
    free(user);
    free(domain);
    free(password);
    free(fullname);
    
    RETURN_FAULT(NULL);
    RETURN_TRUE;
}
/* }}} */

/* {{{ vpop.deluser */
static xmlrpc_value *vpop_deluser(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    char *user, *domain;
    ICONV_DEFT;

    xmlrpc_parse_value(env, param, "({s:s,s:s,*})",
            "user",     &user,
            "domain",   &domain
    );
    RETURN_FAULT(NULL);

    VAUTH_OPEN(1);
    RETURN_FAULT(NULL);

    ICONV_OPEN(0);
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(user);
    IF_FAULT() {
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(domain);
    IF_FAULT() {
        free(user);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_VFAULT(vdeluser(user, domain));

    ICONV_CLOSE();
    VAUTH_CLOSE();

    free(user);
    free(domain);

    RETURN_FAULT(NULL);
    RETURN_TRUE;
}
/* }}} */

/* {{{ vpop.passwd */
static xmlrpc_value *vpop_passwd(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    char *user, *domain, *password;
    ICONV_DEFT;

    xmlrpc_parse_value(env, param, "({s:s,s:s,s:s,*})",
            "user",     &user,
            "domain",   &domain,
            "password", &password
    );
    RETURN_FAULT(NULL);

    VAUTH_OPEN(1);
    RETURN_FAULT(NULL);
    
    ICONV_OPEN(0);
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(user);
    IF_FAULT() {
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }

    ICONV(domain);
    IF_FAULT() {
        free(user);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(password);
    IF_FAULT() {
        free(user);
        free(domain);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_PASS(password);
    IF_FAULT() {
        free(user);
        free(domain);
        free(password);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_VFAULT(vpasswd(user, domain, password, VPOP_XMLRPC_APOP));

    free(user);
    free(domain);
    free(password);

    ICONV_CLOSE();
    VAUTH_CLOSE();
    
    RETURN_FAULT(NULL);
    RETURN_TRUE;
}
/* }}} */

/* {{{ vpop.setquota */
static xmlrpc_value *vpop_setquota(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    char *user, *domain, *quota;
    ICONV_DEFT;

    xmlrpc_parse_value(env, param, "({s:s,s:s,s:s,*})",
            "user",     &user,
            "domain",   &domain,
            "quota",    &quota
    );
    RETURN_FAULT(NULL);

    VAUTH_OPEN(1);
    RETURN_FAULT(NULL);

    ICONV_OPEN(0);
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(user);
    IF_FAULT() {
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(domain);
    IF_FAULT() {
        free(user);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(quota);
    IF_FAULT() {
        free(user);
        free(domain);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_VFAULT(vsetuserquota(user, domain, quota));

    free(user);
    free(domain);
    free(quota);

    ICONV_CLOSE();
    VAUTH_CLOSE();
    
    RETURN_FAULT(NULL);
    RETURN_TRUE;
}
/* }}} */

/* {{{ vpop.auth */
static xmlrpc_value *vpop_auth(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    return auth(env, param, data, 1);
}
/* }}} */

/* {{{ vpop.checkauth */
static xmlrpc_value *vpop_checkauth(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    return auth(env, param, data, 0);
}
/* }}} */

/* {{{ vpop.listusers */
static xmlrpc_value *vpop_listusers(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    char *domain;
    xmlrpc_int32 limit, offset, limit_counter = -1, offset_counter = -1;
    xmlrpc_bool sort;
    uid_t pw_uid;
    gid_t pw_gid;
    struct vqpasswd *mypw;
    xmlrpc_value *users;
    ICONV_DEFT;

    xmlrpc_parse_value(env, param, "({s:s,s:i,s:i,s:b,*})",
            "domain",   &domain,
            "limit",    &limit,
            "offset",   &offset,
            "sort",     &sort
    );
    RETURN_FAULT(NULL);

    VAUTH_OPEN(0);
    RETURN_FAULT(NULL);

    ICONV_OPEN(0);
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }

    ICONV(domain);
    ICONV_CLOSE();
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_FAULT(!vget_assign(domain, NULL, 0, &pw_uid, &pw_gid), VA_DOMAIN_DOES_NOT_EXIST * -1, verror(VA_DOMAIN_DOES_NOT_EXIST));
    IF_FAULT() {
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }

    CHECK_FAULT(setgid(pw_gid), VPOP_XMLRPC_E_SETGID, "setgid() failed");
    IF_FAULT() {
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_FAULT(setuid(pw_uid), VPOP_XMLRPC_E_SETUID, "setuid() failed");
    IF_FAULT() {
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }

    users = xmlrpc_struct_new(env);
    IF_FAULT() {
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }

    
    ICONV_OPEN(1);
    IF_FAULT() {
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }

    while (mypw = vauth_getall(domain, !++offset_counter, sort)) {
        if (offset && (offset_counter < offset)) {
            continue;
        }
        if (limit && (++limit_counter >= limit)) {
            break;
        }
        if (!encode_pw(env, ICONV_PASS, domain, mypw, users)) {
            IF_FAULT() {
                free(domain);
                ICONV_CLOSE();
                vauth_end_getall();
                VAUTH_CLOSE();
                return NULL;
            }
        }
    }

    free(domain);
    ICONV_CLOSE();
    vauth_end_getall();
    VAUTH_CLOSE();

    RETURN_FAULT(NULL);
    return users;
}
/* }}} */

/* {{{ vpop.listdomains */
static xmlrpc_value *vpop_listdomains(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    ICONV_DEFT;
    struct domentry *de, *de_ptr;
    xmlrpc_value *domains; 
    
    domains = xmlrpc_struct_new(env);
    RETURN_FAULT(NULL);
    
    de = get_domain(env, NULL);
    RETURN_FAULT(NULL);
    de_ptr = de;

    ICONV_OPEN(1);
    IF_FAULT() {
        free_domentry(de);
        return NULL;
    }

    while (de->name) {
        encode_de(env, ICONV_PASS, de, domains);
        IF_FAULT() {
            free_domentry(de_ptr);
            ICONV_CLOSE();
            return NULL;
        }
        de = de->next;
    }

    free_domentry(de_ptr);
    ICONV_CLOSE();
    
    RETURN_FAULT(NULL);
    return domains;
}
/* }}} */

/* {{{ vpop.listuser */
static xmlrpc_value *vpop_listuser(xmlrpc_env *env,  xmlrpc_value *param, void *data)
{
    char *domain, *user;
    struct vqpasswd *mypw;
    uid_t pw_uid;
    gid_t pw_gid;
    xmlrpc_value *u;
    ICONV_DEFT;

    xmlrpc_parse_value(env, param, "({s:s,s:s,*})",
            "user",     &user,
            "domain",   &domain
    );
    RETURN_FAULT(NULL);

    VAUTH_OPEN(0);
    RETURN_FAULT(NULL);

    ICONV_OPEN(0);
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(user);
    IF_FAULT() {
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(domain);
    IF_FAULT() {
        free(user);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    ICONV_CLOSE();
    
    CHECK_FAULT(!vget_assign(domain, NULL, 0, &pw_uid, &pw_gid), VA_DOMAIN_DOES_NOT_EXIST * -1, verror(VA_DOMAIN_DOES_NOT_EXIST));
    IF_FAULT() {
        free(user);
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_FAULT(setgid(pw_gid), VPOP_XMLRPC_E_SETGID, "setgid() failed");
    IF_FAULT() {
        free(user);
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_FAULT(setuid(pw_uid), VPOP_XMLRPC_E_SETUID, "setuid() failed");
    IF_FAULT() {
        free(user);
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }
    
    CHECK_FAULT(!(mypw = vauth_getpw(user, domain)), VA_USER_DOES_NOT_EXIST * -1, verror(VA_USER_DOES_NOT_EXIST));
    IF_FAULT() {
        free(user);
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }

    ICONV_OPEN(1);
    IF_FAULT() {
        free(user);
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }
    
    u = encode_pw(env, ICONV_PASS, domain, mypw, NULL);

    free(user);
    free(domain);

    ICONV_CLOSE();
    VAUTH_CLOSE();
    
    RETURN_FAULT(NULL);
    return u;
}
/* }}} */

/* {{{ vpop.listdomain */
static xmlrpc_value *vpop_listdomain(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    char *domain;
    struct domentry *de;
    xmlrpc_value *d;
    ICONV_DEFT;

    xmlrpc_parse_value(env, param, "({s:s,*})", "domain", &domain);
    RETURN_FAULT(NULL);

    ICONV_OPEN(0);
    RETURN_FAULT(NULL);
    
    ICONV(domain);
    IF_FAULT() {
        ICONV_CLOSE();
        return NULL;
    }
    
    ICONV_CLOSE();

    de = get_domain(env, domain);
    free(domain);
    RETURN_FAULT(NULL);
    
    ICONV_OPEN(1);
    IF_FAULT() {
        free_domentry(de);
        return NULL;
    }

    d = encode_de(env, ICONV_PASS, de, NULL);
    free_domentry(de);

    ICONV_CLOSE();
    RETURN_FAULT(NULL);

    return d;
}
/* }}} */

/* {{{ vpop.moduser */
static xmlrpc_value *vpop_moduser(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    char *user, *domain, *fullname = NULL;
    xmlrpc_value *params, *xflags, *xfullname;
    xmlrpc_int32 flags = -1;
    struct vqpasswd *mypw;
    ICONV_DEFT;

    xmlrpc_parse_value(env, param, "(S)", &params);
    RETURN_FAULT(NULL);

    xmlrpc_parse_value(env, params, "{s:s,s:s,*}",
            "user",     &user,
            "domain",   &domain
    );
    RETURN_FAULT(NULL);

    if (xmlrpc_struct_has_key_n(env, params, "flags", sizeof("flags")-1)) {
        xmlrpc_parse_value(env, params, "{s:i,*}", "flags", &flags);
    }
    RETURN_FAULT(NULL);
    
    if (xmlrpc_struct_has_key_n(env, params, "fullname", sizeof("fullname")-1)) {
        xmlrpc_parse_value(env, params, "{s:s,*}", "fullname", &fullname);
    }
    RETURN_FAULT(NULL);

    if ((flags < 0) && (!fullname)) {
        RAISE_FAULT(VPOP_XMLRPC_E_BADCALL, "fullname and/or flags are expected as paramters");
        return NULL;
    }
    
    VAUTH_OPEN(1);
    RETURN_FAULT(NULL);
    
    ICONV_OPEN(0);
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }

    ICONV(user);
    IF_FAULT() {
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }

    ICONV(domain);
    IF_FAULT() {
        free(user);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }

    if (fullname) {
        ICONV(fullname);
        IF_FAULT() {
            free(user);
            free(domain);
            ICONV_CLOSE();
            VAUTH_CLOSE();
            return NULL;
        }
    }

    ICONV_CLOSE();

    CHECK_FAULT(!(mypw = vauth_getpw(user, domain)), VA_USER_DOES_NOT_EXIST * -1, verror(VA_USER_DOES_NOT_EXIST));
    IF_FAULT() {
        if (fullname) {
            free(fullname);
        }
        free(user);
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }
    
    if (fullname) {
        mypw->pw_gecos = fullname;
    }
    if (flags >= 0) {
        mypw->pw_flags = flags;
    }

    CHECK_VFAULT(vauth_setpw(mypw, domain));
    
    free(domain);
    free(user);
    if (fullname) {
        free(fullname);
    }

    VAUTH_CLOSE();

    RETURN_FAULT(NULL);
    RETURN_TRUE;
}
/* }}} */

/* {{{ vpop.setflags */
static xmlrpc_value *vpop_setflags(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    int i;
    char *user, *domain;
    xmlrpc_value *params;
    struct vqpasswd *mypw;
    ICONV_DEFT;

    xmlrpc_parse_value(env, param, "(S)", &params);
    RETURN_FAULT(NULL);

    xmlrpc_parse_value(env, params, "{s:s,s:s,*}",
            "user",     &user,
            "domain",   &domain
    );
    RETURN_FAULT(NULL);

    VAUTH_OPEN(1);
    RETURN_FAULT(NULL);

    ICONV_OPEN(0);
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }

    ICONV(user);
    IF_FAULT() {
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }

    ICONV(domain);
    IF_FAULT() {
        free(user);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }

    ICONV_CLOSE();

    CHECK_FAULT(!(mypw = vauth_getpw(user, domain)), VA_USER_DOES_NOT_EXIST * -1, verror(VA_USER_DOES_NOT_EXIST));
    
    free(user);
    
    IF_FAULT() {
        free(domain);
        VAUTH_CLOSE();
        return NULL;
    }

    for (i = 0; i < (sizeof(flags) / sizeof(struct vpop_xmlrpc_flag)); ++i) {
        xmlrpc_bool flag;
        
        if (xmlrpc_struct_has_key_n(env, params, flags[i].name, flags[i].nlen)) {
            xmlrpc_parse_value(env, params, "{s:b,*}", flags[i].name, &flag);
            
            IF_FAULT() {
                free(domain);
                VAUTH_CLOSE();
                return NULL;
            }
            
            if (flag) {
                mypw->pw_flags |= flags[i].flag;
            } else {
                mypw->pw_flags &=~ flags[i].flag;
            }
        }
        
        IF_FAULT() {
            free(domain);
            VAUTH_CLOSE();
            return NULL;
        }
    }

    CHECK_VFAULT(vauth_setpw(mypw, domain));

    free(domain);
    VAUTH_CLOSE();

    RETURN_FAULT(NULL);
    RETURN_TRUE;
}
/* }}} */

/* {{{ vpop.listflags */
static xmlrpc_value *vpop_listflags(xmlrpc_env *env, xmlrpc_value *param, void *data)
{
    int i;
    xmlrpc_value *f;
    
    f = xmlrpc_struct_new(env);
    RETURN_FAULT(NULL);
    
    for (i = 0; i < (sizeof(flags) / sizeof(struct vpop_xmlrpc_flag)); ++i) {
        xmlrpc_value *v;
        
        v = xmlrpc_int_new(env, flags[i].flag);
        RETURN_FAULT(NULL);

        xmlrpc_struct_set_value(env, f, flags[i].name, v);
        RETURN_FAULT(NULL);
    }

    return f;
}
/* }}} */

/* {{{ main */
int main(int argc, char *argv[])
{
    /* if invoked with an argument, start abyss server */
    if (argc > 1) {
        fprintf(stderr, "Initializing Abyss... ");
        xmlrpc_server_abyss_init(XMLRPC_SERVER_ABYSS_NO_FLAGS, argv[1]);
        fprintf(stderr, "done\n");
        
        /* Install methods */
        {
            int i;
            
            fprintf(stderr, "Installing methods:\n");
            
            for (i = 0; i < (sizeof(me) / sizeof(struct xmlrpc_method_entry)); ++i) {
                fprintf(stderr, "\t%20s()... ", me[i].name);
                xmlrpc_server_abyss_add_method_w_doc(me[i].name, me[i].func, me[i].data, me[i].spec, me[i].docs);
                fprintf(stderr, "done\n");
            }
        }
        fprintf(stderr, "Switching to background...\n");
        xmlrpc_server_abyss_run();
    
    /* else run CGI */
    } else {
        xmlrpc_cgi_init(XMLRPC_CGI_NO_FLAGS);
        
        /* Install methods */
        {
            int i;
            for (i = 0; i < (sizeof(me) / sizeof(struct xmlrpc_method_entry)); ++i) {
                xmlrpc_cgi_add_method_w_doc(me[i].name, me[i].func, me[i].data, me[i].spec, me[i].docs);
            }
        }

        xmlrpc_cgi_process_call();
        xmlrpc_cgi_cleanup();
    }
    
    return 0;
}
/* }}} */

/* {{{ static inline void batch_fault(xmlrpc_env *, xmlrpc_value *) */
static inline int batch_fault(xmlrpc_env *env, xmlrpc_int32 c, xmlrpc_value *array)
{
    char *err_msg;
    xmlrpc_int32 err_code;
    xmlrpc_value *err_struct, *v;
    
    err_msg = strdup(env->fault_string);
    err_code = env->fault_code;
    
    env->fault_occurred = 0;
    
    err_struct = xmlrpc_struct_new(env);
    IF_FAULT() {
        free(err_msg);
        return 1;
    }
    
    v = xmlrpc_int_new(env, c);
    IF_FAULT() {
        free(err_msg);
        return 1;
    }
    xmlrpc_struct_set_value(env, err_struct, "batchno", v);
    IF_FAULT() {
        free(err_msg);
        return 1;
    }
    
    v = xmlrpc_int_new(env, err_code);
    IF_FAULT() {
        free(err_msg);
        return 1;
    }
    xmlrpc_struct_set_value(env, err_struct, "fault_code", v);
    IF_FAULT() {
        free(err_msg);
        return 1;
    }

    v = xmlrpc_string_new(env, err_msg);
    IF_FAULT() {
        free(err_msg);
        return 1;
    }
    xmlrpc_struct_set_value(env, err_struct, "fault_string", v);
    IF_FAULT() {
        free(err_msg);
        return 1;
    }

    xmlrpc_array_append_item(env, array, err_struct);
    IF_FAULT() {
        free(err_msg);
        return 1;
    }

    free(err_msg);
    return 0;
}
/* }}} */

/* {{{ static struct xmlrpc_method_entry *me_entry(xmlrpc_env *, char *) */
static struct xmlrpc_method_entry *me_entry(xmlrpc_env *env, char *name)
{
    int i;
    for (i = 0; i < (sizeof(me) / sizeof(struct xmlrpc_method_entry)); ++i) {
        if (!strcmp(me[i].name + sizeof("vpop"), name)) {
            return &me[i];
        }
    }
    xmlrpc_env_set_fault_formatted(env, VPOP_XMLRPC_E_BADCALL, "Unkown method vpop.%s", name);
    return NULL;
}
/* }}} */

/* {{{ static inline xmlrpc_value *auth(xmlrpc_env *, xmlrpc_value *, void *, int) */
static inline xmlrpc_value *auth(xmlrpc_env *env, xmlrpc_value *param, void *data, int log_auth)
{
    struct vqpasswd *mypw;
    char *user, *domain, *password;
    ICONV_DEFT;
    int authenticated;

    xmlrpc_parse_value(env, param, "({s:s,s:s,s:s,*})",
            "user",     &user,
            "domain",   &domain,
            "password", &password
    );
    RETURN_FAULT(NULL);

    VAUTH_OPEN(
#ifdef ENABLE_AUTH_LOGGING
        log_auth
#else
        0
#endif
    );
    RETURN_FAULT(NULL);

    ICONV_OPEN(0);
    IF_FAULT() {
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(user);
    IF_FAULT() {
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(domain);
    IF_FAULT() {
        free(user);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    ICONV(password);
    IF_FAULT() {
        free(user);
        free(domain);
        ICONV_CLOSE();
        VAUTH_CLOSE();
        return NULL;
    }
    
    mypw = vauth_getpw(user, domain);
    /* old users may have been added with other CLEAR_PASS setting */
    if (mypw && mypw->pw_clear_passwd && strlen(mypw->pw_clear_passwd) && (strlen(password) >= MAX_PW_CLEAR_PASSWD)) {
        password[MAX_PW_CLEAR_PASSWD - 1] = 0;
    }
    authenticated = !vauth_crypt(user, domain, password, mypw);

#ifdef ENABLE_AUTH_LOGGING
    if (authenticated && log_auth) {
        vset_lastauth(user, domain, getenv("REMOTE_ADDR"));
    }
#endif

    free(user);
    free(domain);
    free(password);

    ICONV_CLOSE();
    VAUTH_CLOSE();
    
    RETURN_FAULT(NULL);
    if (authenticated) {
        RETURN_TRUE;
    } else {
        RETURN_FALSE;
    }
}
/* }}}*/

/* {{{ static inline int get_domusers(char *, uid_t, gid_t) */
static inline int get_domusers(char *domain, uid_t uid, gid_t gid)
{
    int count = 0;
    open_big_dir(domain, uid, gid);
    count = vdir.cur_users;
    close_big_dir(domain, uid, gid);
    return count;
}
/* }}} */

/* {{{ static inline void free_domentry(struct domentry *e) */
static inline void free_domentry(struct domentry *e)
{
    if (e) {
        if (e->name) {
            free(e->name);
        }
        if (e->real) {
            free(e->real);
        }
        if (e->next) {
            free_domentry(e->next);
        }
        free(e);
    }
}
/* }}} */

/* {{{ static struct domentry *get_domain(xmlrpc_env *, char *) */
static struct domentry *get_domain(xmlrpc_env *env, char *domain)
{
    FILE *fs = NULL;
    char path[255] = {0}, line[1024], *ptr;
    struct domentry *e = calloc(1, sizeof(struct domentry)), *e_ptr = e;

    snprintf(path, 254, "%s/users/assign", QMAILDIR);
    if (!(fs = fopen(path, "r"))) {
            free(e);
            RAISE_FAULT(VPOP_XMLRPC_E_ASSIGN, "Could not open qmail assign file.");
            return NULL;
    }

    while (fgets(line, 1023, fs)) {
        char *dom, *real;
        int uid, gid;

        if (*line != '+') {
            continue;
        }

        if ((!(dom = strtok(line + 1, ":"))) || (!strchr(dom, '.'))) {
            continue;
        }
        if (!(real = strtok(NULL, ":"))) {
            continue;
        } else {
            *(real - 2) = 0;
        }
        if (!(ptr = strtok(NULL, ":"))) {
            continue;
        } else {
            uid = atoi(ptr);
        }
        if (!(ptr = strtok(NULL, ":"))) {
            continue;
        } else {
            gid = atoi(ptr);
        }

        if (domain) {
            if (!strcmp(domain, dom)) {
                e->name = strdup(dom);
                e->real = strdup(real);
                e->uid = uid;
                e->gid = gid;
                e->alias = strcmp(dom, real) ? 1:0;
                e->users = get_domusers(real, uid, gid);

                fclose(fs);
                return e;
            }
        } else {
            e_ptr->name = strdup(dom);
            e_ptr->real = strdup(real);
            e_ptr->uid = uid;
            e_ptr->gid = gid;
            e_ptr->alias = strcmp(dom, real) ? 1:0;
            e_ptr->users = get_domusers(real, uid, gid);
            e_ptr->next = calloc(1, sizeof(struct domentry));
            e_ptr = e_ptr->next;
        }
    }

    fclose(fs);
    
    if (e->name) {
        return e;
    } else {
        free(e);
        RAISE_FAULT(VA_DOMAIN_DOES_NOT_EXIST * -1, verror(VA_DOMAIN_DOES_NOT_EXIST));
        return NULL;
    }
}
/* }}} */

/* {{{ static inline xmlrpc_value *encode_pw(xmlrpc_env *, iconv_t, char *, struct vqpasswd *, xmlrpc_value *) */
static inline xmlrpc_value *encode_pw(xmlrpc_env *env, ICONV_DEFT, char *domain, struct vqpasswd *mypw, xmlrpc_value *into_struct)
{
    xmlrpc_value *pw, *v;
    char *pw_name, *pw_clear_passwd, *pw_gecos;
    
    pw = xmlrpc_struct_new(env);
    RETURN_FAULT(NULL);
    
    pw_name = strdup(mypw->pw_name);
    pw_gecos = strdup(mypw->pw_gecos);
    pw_clear_passwd = strdup(mypw->pw_clear_passwd);

    ICONV(pw_name);
    IF_FAULT() {
        free(pw_name);
        free(pw_gecos);
        free(pw_clear_passwd);
        return NULL;
    }
    
    ICONV(pw_gecos);
    IF_FAULT() {
        free(pw_name);
        free(pw_gecos);
        free(pw_clear_passwd);
        return NULL;
    }
    
    ICONV(pw_clear_passwd);
    IF_FAULT() {
        free(pw_name);
        free(pw_gecos);
        free(pw_clear_passwd);
        return NULL;
    }

    if (into_struct) {
        xmlrpc_struct_set_value(env, into_struct, pw_name, pw);
        IF_FAULT() {
            free(pw_name);
            free(pw_gecos);
            free(pw_clear_passwd);
            return NULL;
        }
    }
    
    v = xmlrpc_string_new(env, pw_name);
    IF_FAULT() {
        free(pw_name);
        free(pw_gecos);
        free(pw_clear_passwd);
        return NULL;
    }
    xmlrpc_struct_set_value(env, pw, "name", v);
    IF_FAULT() {
        free(pw_name);
        free(pw_gecos);
        free(pw_clear_passwd);
        return NULL;
    }
    
    v = xmlrpc_string_new(env, pw_gecos);
    IF_FAULT() {
        free(pw_name);
        free(pw_gecos);
        free(pw_clear_passwd);
        return NULL;
    }
    xmlrpc_struct_set_value(env, pw, "fullname", v);
    IF_FAULT() {
        free(pw_name);
        free(pw_gecos);
        free(pw_clear_passwd);
    }
    
    v = xmlrpc_string_new(env, pw_clear_passwd);
    IF_FAULT() {
        free(pw_name);
        free(pw_gecos);
        free(pw_clear_passwd);
        return NULL;
    }
    xmlrpc_struct_set_value(env, pw, "clearpassword", v);
    IF_FAULT() {
        free(pw_name);
        free(pw_gecos);
        free(pw_clear_passwd);
        return NULL;
    }
    
    free(pw_name);
    free(pw_gecos);
    free(pw_clear_passwd);
    
    v = xmlrpc_string_new(env, mypw->pw_passwd);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, pw, "password", v);
    RETURN_FAULT(NULL);
    
    v = xmlrpc_int_new(env, mypw->pw_uid);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, pw, "uid", v);
    RETURN_FAULT(NULL);
    
    v = xmlrpc_int_new(env, mypw->pw_gid);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, pw, "gid", v);
    RETURN_FAULT(NULL);
    
    v = xmlrpc_int_new(env, mypw->pw_flags);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, pw, "flags", v);
    RETURN_FAULT(NULL);
    
    v = xmlrpc_string_new(env, mypw->pw_shell);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, pw, "quota", v);
    RETURN_FAULT(NULL);

    if (!strcmp(mypw->pw_shell, "NOQUOTA")) {
        xmlrpc_struct_set_value(env, pw, "usage", v);
        RETURN_FAULT(NULL);
    } else {
        char quota[5], maildir[256];
        
        snprintf(maildir, 255, "%s/Maildir", mypw->pw_dir);
        snprintf(quota, 4, "%d%%", vmaildir_readquota(maildir, format_maildirquota(mypw->pw_shell)));
        
        v = xmlrpc_string_new(env, quota);
        RETURN_FAULT(NULL);
        xmlrpc_struct_set_value(env, pw, "usage", v);
        RETURN_FAULT(NULL);
    }
    
#ifdef ENABLE_AUTH_LOGGING
    {
        char *authip = vget_lastauthip(mypw, domain);
        time_t authtime = vget_lastauth(mypw, domain);
        
        if ((!authtime) || (!authip) || (!strcmp(authip, "0.0.0.0"))) {
            v = xmlrpc_string_new(env, "Never logged in");
            RETURN_FAULT(NULL);
            xmlrpc_struct_set_value(env, pw, "lastauth", v);
            RETURN_FAULT(NULL);
        } else {
            v = xmlrpc_string_new(env, asctime(localtime(&authtime)));
            RETURN_FAULT(NULL);
            xmlrpc_struct_set_value(env, pw, "lastauth", v);
            RETURN_FAULT(NULL);
        }
    }
#else
    v = xmlrpc_string_new(env, "Not available");
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, pw, "lastauth", v);
    RETURN_FAULT(NULL);
#endif

    return pw;
}
/* }}} */

/* {{{ static inline xmlrpc_value *encode_de(xmlrpc_env *, iconv_t, struct domentry *, xmlrpc_value *) */
static inline xmlrpc_value *encode_de(xmlrpc_env *env, ICONV_DEFT, struct domentry *de, xmlrpc_value *into_struct)
{
    char *name, *real;
    xmlrpc_value *dom, *v;
    
    dom = xmlrpc_struct_new(env);
    RETURN_FAULT(NULL);
    
    name = strdup(de->name);
    real = strdup(de->real);

    ICONV_FREE(name);
    IF_FAULT() {
        free(name);
        free(real);
        return NULL;
    }

    ICONV_FREE(real);
    IF_FAULT() {
        free(name);
        free(real);
        return NULL;
    }

    if (into_struct) {
        xmlrpc_struct_set_value(env, into_struct, name, dom);
        IF_FAULT() {
            free(name);
            free(real);
            return NULL;
        }
    }

    v = xmlrpc_string_new(env, name);
    IF_FAULT() {
        free(name);
        free(real);
        return NULL;
    }
    xmlrpc_struct_set_value(env, dom, "name", v);
    IF_FAULT() {
        free(name);
        free(real);
        return NULL;
    }
    
    v = xmlrpc_string_new(env, real);
    IF_FAULT() {
        free(name);
        free(real);
        return NULL;
    }
    xmlrpc_struct_set_value(env, dom, "real", v);
    IF_FAULT() {
        free(name);
        free(real);
        return NULL;
    }
    
    free(name);
    free(real);
    
    v = xmlrpc_int_new(env, de->alias);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, dom, "alias", v);
    RETURN_FAULT(NULL);
    
    v = xmlrpc_int_new(env, de->uid);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, dom, "uid", v);
    RETURN_FAULT(NULL);
    
    v = xmlrpc_int_new(env, de->gid);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, dom, "gid", v);
    RETURN_FAULT(NULL);
    
    v = xmlrpc_int_new(env, de->users);
    RETURN_FAULT(NULL);
    xmlrpc_struct_set_value(env, dom, "users", v);
    RETURN_FAULT(NULL);
    
    return dom;
}
/* }}} */

/* -- END -- */

