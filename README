README: vpop-xmlrpc
===================
$Id$

INSTALLATION:
~~~~~~~~~~~~~

Run ./configure --help for available compile options.

Typical installation:
$ ./configure \
    --with-qmail-dir=/var/qmail \
    --with-vpopmail=/usr \
    --with-xmlrpc=/usr
$ make
$ sudo make install

# using as CGI:
sudo cp /usr/bin/vpop-xmlrpc /usr/lib/cgi-bin
sudo chown vpopmail.vhckpw /usr/lib/cgi-bin/vpop-xmlrpc
sudo chmod u+s,g+s /usr/lib/cgi-bin/vpop-xmlrpc

# using as standalone server
# see xmlrpc-c docs for config file documentation
su vpopmail -c '/usr/bin/vpop-xmlrpc /etc/vpop-xmlrpc'


DON'T FORGET TO RESTRICT ACCESS TO vpop-xmlrpc!


USAGE:
~~~~~~

Currently available methods are:

    * vpop.adduser
        Expects a struct as parameter containing following members:
            o user:     string, the user part of the email address
            o domain:   string, the domain part of the email address
            o password: string, the plaintext password of the account
            o fullname: string, the full name (used for the gecos field)
        Returns bool.

    * vpop.deluser
        Expects a struct as parameter containing following memebers:
            o user:     string, the user part of the email address
            o domain:   string, the domain part of the email address
        Returns bool.

    * vpop.passwd
        Expects a struct as parameter containing following members:
            o user:     string, the user part of the email address
            o domain:   string, the domain part of the email address
            o password: string, the new plaintext password of the account
        Returns bool.

    * vpop.setquota
        Expects a struct as parameter containing the following members:
           o user:      string, the user part of the email address
           o domain:    string, the domain part of the email address
           o quota:     string, the quota to apply to the account
                        (usual formats like 1M apply)
       Returns bool.

    * vpop.auth
        Expects a struct as parameter containing the following members:
            o user:     string, the user part of the email address
            o domain:   string, the domain part of the email address
            o password: string, the plaintext password of the user
        Returns bool.

    * vpop.listusers
        Expects a struct as parameter containing the following members:
            o domain:   string, the domain of which the users should be listed
            o limit:    int, the count of users to return
            o offset:   int, the offset from where to start
            o sort:     bool, whether to sort users by their mailbox
        Returns a struct listing all users by their mailbox' name.

    * vpop.listdomains
        Expects no parameter.
        Returns a struct listing all domains by their name.

    * vpop.moduser
        Expects a struct as paramter containing the following members:
            o user:     string, the user part of the email address
            o domain:   string, the domain part of the email address
            o fullname: string, (optional) new fullname (gecos)
            o flags:    int, (optional) new flags
        Returns bool.

    * vpop.setflags
        Expects a struct as paramter containing the following members:
            o user:     string, the user part of the email address
            o domain:   string, the domain part of the email address
            o and any of the following bools:
                - no_password_change
                - no_pop_access
                - no_smtp_auth_access
                - no_local_imap_access
                - no_remote_imap_access
                - do_bounce_all
                - no_domain_limits
                - no_roaming
                - is_qmail_admin
                - is_system_admin (may not be available)
                - is_expert_admin (may not be available)
                - no_spamassassin (may not be available)
                - do_delete_spam  (may not be available)
        Returns bool.

    * vpop.listflags
        Expects no parameter.
        Returns a struct listing all setable flags with their name and
        numerical (int) representation.

END
